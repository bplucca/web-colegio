from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'webcolegio/index.html', {})

def perfil(request):
    return render(request, 'webcolegio/perfil.html', {})

def cursos(request):
    return render(request, 'webcolegio/cursos.html', {})